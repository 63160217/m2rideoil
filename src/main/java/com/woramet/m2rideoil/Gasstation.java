/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.woramet.m2rideoil;

/**
 *
 * @author User
 */
public class Gasstation {
    protected String service;
    protected int care = 0;

    public Gasstation(String service,int care) {
        this.service = service;
        //care : 0 is self service.
        //care : 1 is service staff.
        this.care = care;
    }
    
    public void detail (){
        System.out.println("Service : " + service);
        System.out.println("Care : " + care);
        System.out.println("-------------------------------");
    }
}
