/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.woramet.m2rideoil;

/**
 *
 * @author User
 */
public class Shop extends Gasstation{
    String name;
    int num;
    public Shop(String name ,int num) {
        super(name,num);
        this.name = name;
        this.num = num;
    }
    @Override
    public void detail(){
        System.out.println("TypeShop : " + name);
        System.out.println("NumShop : " + num);
        System.out.println("-------------------------------");
    }
}
