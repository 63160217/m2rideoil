/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.woramet.m2rideoil;

/**
 *
 * @author User
 */
public class Inflator extends Gasstation{
    private int num;
    public Inflator(String service, int num) {
        super(service, num);
        this.num = num;
    }
    
    @Override
    public void detail(){
        System.out.println("Service : " + service);
        System.out.println("NumInflator : " + num);
        System.out.println("-------------------------------");
    }
}
