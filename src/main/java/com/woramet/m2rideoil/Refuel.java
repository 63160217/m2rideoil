/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.woramet.m2rideoil;

/**
 *
 * @author User
 */
public class Refuel extends Gasstation{
    String name;
    int num;
    double rates;
    public Refuel(String name, int num ,double rates) {
        super(name, num);
        this.name = name;
        this.num = num;
        this.rates = rates;
    }
    
    @Override
    public void detail(){
        System.out.println("TypeOil : " + name);
        System.out.println("NumCabinet : " + num);
        System.out.println("RatesOil : " + rates + " baht");
        System.out.println("-------------------------------");
    }
    
}
